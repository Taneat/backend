const express = require('express')
const router = express.Router()
const jobseekerController = require('../controller/JobseekerController')

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.json(jobseekerController.getUsers())
})
router.get('/:id', (req, res, next) => {
  const { id } = req.params
  res.json(jobseekerController.getUser(id))
})
router.post('/', (req, res, next) => {
  const payload = req.body
  res.json(jobseekerController.addUser(payload))
})
router.put('/', (req, res, next) => {
  const payload = req.body
  res.json(jobseekerController.updateUser(payload))
})
router.delete('/:id', (req, res, next) => {
  const { id } = req.params
  res.json(jobseekerController.deleteUser(id))
})

module.exports = router
