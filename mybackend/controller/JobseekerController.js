const jobseekerController = {
  userList: [
    { id: 1, fname: 'Taneat', lname: 'Taddsab', idcard: '1258866477859', gender: 'M', age: 20, address: 'chonburi', tel: '0999999999' },
    { id: 2, fname: 'Saharud', lname: 'Seerakoon', idcard: '1278899654258', gender: 'M', age: 20, address: 'chonburi', tel: '0911111111' },
    { id: 3, fname: 'Siriphonpha', lname: 'Fangnakham', idcard: '1276633211458', gender: 'F', age: 20, address: 'chonburi', tel: '0987654321' }
  ],
  listId: 3,
  addUser (user) {
    user.id = this.listId++
    this.userList.push(user)
    return user
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
    return user
  },
  deleteUser (id) {
    const index = this.userList.findIndex(item => item.id === parseInt(id))
    this.userList.splice(index, 1)
    return { id }
  },
  getUsers () {
    return [...this.userList]
  },
  getUser (id) {
    const user = this.userList.find(item => item.id === parseInt(id))
    return user
  }
}
module.exports = jobseekerController
